import * as express from "express"
import { Server } from "http";
import { join } from "path";
import * as SocketIO from "socket.io"

const port = process.env.PORT || 3000
const app = express()
const http = new Server(app)
const io = SocketIO(http)

app.use(express.static(join(__dirname, "dist")))

io.on("connection", socket => {
    console.log("A client has connected")
})

http.listen(port, () => console.log("Server has started"))