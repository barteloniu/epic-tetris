import "./style/main.scss"
import * as io from "socket.io-client"
import { Application, loader } from "pixi.js";
import { Shape } from "./classes";

const socket = io()

export let width=300
export let height=600
export let bs=width/10

if(window.innerWidth<800){
    width=window.innerWidth-100
    height=(window.innerWidth-100)*2
}

export const app = new Application({
    width: width,
    height: height,
    antialias: true
})
document.body.appendChild(app.view)

const setup = (): void => {
    //testing shapes
    new Shape("I",bs,bs)
    new Shape("J",bs,bs)
    new Shape("L",bs,bs)
    new Shape("O",bs,bs)
    new Shape("S",bs,bs)
    new Shape("T",bs,bs)
    new Shape("Z",bs,bs)
    // app.ticker.add(delta => gameloop(delta))
}

const gameloop = (delta: number): void => {
    //cat.rotation += DEG_TO_RAD * 1
}

loader
.add("I","images/I.png")
.add("J","images/J.png")
.add("L","images/L.png")
.add("O","images/O.png")
.add("S","images/S.png")
.add("T","images/T.png")
.add("Z","images/Z.png")
.load(setup)
