import { app, bs } from "./index"
import { Application, Sprite, utils } from "pixi.js"

export class Shape{
    name: string 
	canvas: Application
    constructor(name,x,y){
		this.name=name
		let block=new Sprite(utils.TextureCache[this.name])
		
		if(this.name=="I"){ 	 block.width=4*bs; block.height=bs; }
		else if(this.name=="O"){ block.width=2*bs; block.height=2*bs; }
		else{ 					 block.width=3*bs; block.height=2*bs; }

		block.x=x
		block.y=y
		app.stage.addChild(block)
	}
}